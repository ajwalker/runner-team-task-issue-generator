package main

import "slices"

type Who struct {
	CoreBugWrangler  string   `json:"core_bug_wrangler"`
	FleetBugWrangler string   `json:"fleet_bug_wrangler"`
	SaasBugWrangler  string   `json:"saas_bug_wrangler"`
	MergeMarauder    string   `json:"merge_marauder"`
	SupportResponder string   `json:"support_responder"`
	CommunityTriager string   `json:"community_triager"`
	Unassigned       []string `json:"unassigned"`

	CoreBugWranglerRotation  []string `json:"core_bug_wrangler_rotation"`
	FleetBugWranglerRotation []string `json:"fleet_bug_wrangler_rotation"`
	SaasBugWranglerRotation  []string `json:"saas_bug_wrangler_rotation"`
	MergeMarauderRotation    []string `json:"merge_marauder_rotation`
}

func (who *Who) Rotate(pto PTO) {
	who.Unassigned = append(who.Unassigned, who.CommunityTriager, who.SupportResponder, who.CoreBugWrangler, who.FleetBugWrangler)
	who.CoreBugWrangler = who.RotateGroup(&who.CoreBugWranglerRotation, who.RemoveUnassigned(who.Prefer("", who.CoreBugWranglerRotation, pto)))
	who.FleetBugWrangler = who.RotateGroup(&who.FleetBugWranglerRotation, who.RemoveUnassigned(who.Prefer("", who.FleetBugWranglerRotation, pto)))
	who.SaasBugWrangler = who.RotateGroup(&who.SaasBugWranglerRotation, who.RemoveUnassigned(who.Prefer("", who.SaasBugWranglerRotation, pto)))
	who.MergeMarauder = who.RotateGroup(&who.MergeMarauderRotation, who.RemoveUnassigned(who.Prefer("", who.MergeMarauderRotation, pto)))
	who.CommunityTriager = who.RemoveUnassigned(who.Prefer(who.SupportResponder, who.Unassigned, pto, who.CoreBugWrangler, who.FleetBugWrangler))
	who.SupportResponder = who.RemoveUnassigned(who.Prefer(who.CoreBugWrangler, who.Unassigned, pto, who.CoreBugWrangler, who.FleetBugWrangler))
}

func (w *Who) Prefer(preferred string, alternative []string, pto PTO, not ...string) string {
	if slices.Contains(not, preferred) {
		preferred = ""
	}

	if preferred != "" && pto[preferred] < minTimeOffToBeExcused {
		return preferred
	}

	if len(alternative) > 0 {
		return w.Prefer(alternative[0], alternative[1:], pto, not...)
	}

	return ""
}

func (w *Who) RemoveUnassigned(user string) string {
	w.Unassigned = slices.DeleteFunc(w.Unassigned, func(v string) bool {
		return v == user
	})

	return user
}

func (w *Who) RotateGroup(group *[]string, user string) string {
	*group = slices.DeleteFunc(*group, func(v string) bool {
		return v == user
	})

	*group = append(*group, user)

	return user
}
