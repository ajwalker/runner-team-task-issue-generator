package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"strings"
	"text/template"
	"time"

	gitlab "github.com/xanzy/go-gitlab"
)

// PTO is a map of users to hours time off during the specified week
type PTO map[string]time.Duration

const (
	minTimeOffToBeExcused = 3 * 24 * time.Hour
	metadataCommentPrefix = "<!-- metadata:"
	metadataCommentSuffix = "-->"

	teamTasksProjectID = 18107757
)

var (
	dryrun    bool
	projectId int

	genesis = time.Date(time.Now().Year(), time.January, 1, 0, 0, 0, 0, time.UTC)
)

func init() {
	flag.BoolVar(&dryrun, "dryrun", false, "dry run")
	flag.IntVar(&projectId, "project", teamTasksProjectID, "project id")
}

func main() {
	client := NewClient(os.Getenv("GITLAB_TOKEN"), projectId)
	who, previousIssue, week, err := client.GetPrevious()
	if err != nil {
		panic(err)
	}

	year, currentWeek := time.Now().ISOWeek()
	// ISOWeek could return the wrong week in some cases.
	if year != time.Now().Year() {
		currentWeek = 53
	}

	// do nothing if latest issue is current
	if !dryrun {
		dryrun = currentWeek == week
	}

	who.Rotate(PTO{}) // todo: PTO

	tmpl := template.New("md")
	tmpl.Funcs(template.FuncMap{
		"json": func(input any) string {
			buf, _ := json.MarshalIndent(input, "", "    ")
			return string(buf)
		},
	})

	buf, _ := os.ReadFile("template.md")
	tmpl, err = tmpl.Parse(string(buf))
	if err != nil {
		panic(err)
	}

	from := genesis.Add((time.Duration(currentWeek-1) * (7 * 24) * time.Hour) + 24*time.Hour)
	// handle leap days
	for from.Weekday() != time.Monday {
		from = from.Add(-24 * time.Hour)
	}
	to := from.Add((time.Duration(7*24) * time.Hour) - time.Minute)

	title := fmt.Sprintf("Week %d - %s - %s - Runner Shared Team Tasks\n", currentWeek, from.Format("Jan 2 2006"), to.Format("Jan 2 2006"))
	description := new(strings.Builder)

	type Data struct {
		Who
		Next Who    `json:"-"`
		Due  string `json:"due"`
	}
	err = tmpl.Execute(description, Data{Who: *who, Due: to.Format("Mon Jan 2 2006")})
	if err != nil {
		panic(err)
	}

	if err := client.CreateIssue(title, description.String()); err != nil {
		panic(err)
	}

	if err := client.CloseIssue(previousIssue); err != nil {
		panic(err)
	}
}

type Client struct {
	client    *gitlab.Client
	projectID int
}

func NewClient(token string, projectId int) *Client {
	client, err := gitlab.NewClient(token)
	if err != nil {
		panic(err)
	}

	return &Client{
		client:    client,
		projectID: projectId,
	}
}

func (c *Client) CreateIssue(title, description string) error {
	fmt.Println(title)
	fmt.Println(description)

	if dryrun {
		fmt.Println("dryrun or issue already exists, so not creating new issue.")
		return nil
	}

	issueOptions := &gitlab.CreateIssueOptions{
		Title:       gitlab.String(title),
		Description: gitlab.String(description),
	}

	_, _, err := c.client.Issues.CreateIssue(c.projectID, issueOptions)
	return err
}

func (c *Client) CloseIssue(issue *gitlab.Issue) error {
	fmt.Println("closing previous issue", issue.Title)

	if dryrun {
		fmt.Println("will not close issue when in dryrun")
		return nil
	}

	issueOptions := &gitlab.UpdateIssueOptions{
		StateEvent: gitlab.String("close"),
	}

	_, _, err := c.client.Issues.UpdateIssue(c.projectID, issue.IID, issueOptions)
	return err
}

func (c *Client) GetPrevious() (*Who, *gitlab.Issue, int, error) {
	listOptions := &gitlab.ListProjectIssuesOptions{
		Search: gitlab.String("Runner Shared Team Tasks"),
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}

	for {
		issues, resp, err := c.client.Issues.ListProjectIssues(c.projectID, listOptions)
		if err != nil {
			return nil, nil, 0, err
		}

		for _, issue := range issues {
			if strings.HasPrefix(issue.Title, "Week") {
				start := strings.Index(issue.Description, metadataCommentPrefix)
				if start == -1 {
					continue
				}

				issue.Description = issue.Description[start+len(metadataCommentPrefix):]
				end := strings.Index(issue.Description, metadataCommentSuffix)
				if end == -1 {
					continue
				}

				issue.Description = issue.Description[:end]

				week := issue.CreatedAt.Sub(genesis) / (7 * 24 * time.Hour)

				var who Who
				return &who, issue, int(week) + 1, json.Unmarshal([]byte(issue.Description), &who)
			}
		}

		if resp.CurrentPage >= resp.TotalPages {
			break
		}

		listOptions.Page = resp.NextPage
	}

	return nil, nil, 0, fmt.Errorf("no issue found")
}
