
# Assignments

| What   | Who   |
| ------ | ----- |
| 🐛 🤠 Core Bug Wrangler              | {{ .CoreBugWrangler }}   |
| 🐛 🤠 Fleet Bug Wrangler             | {{ .FleetBugWrangler }}  |
| 🐛 🤠 Saas Bug Wrangler              | {{ .SaasBugWrangler }}   |
| 🛟 🚒 Support & Security Responder     | {{ .SupportResponder }}  |
| 🥷 🏴‍☠️ Merge Marauder                 | {{ .MergeMarauder }} |
| 💬 📌 Community Contribution Triager | {{ .CommunityTriager }}  |
| 💤 💤 Unassigned this week           | {{range $idx, $val := .Unassigned}}{{if $idx}}, {{end}}`{{ $val }}`{{end}} |

# Duties

## 🐛 Bug Wranglers 🤠

- [ ] Triage all bugs so that they have the appropriate `~severity::*` and `~priority::*` labels.
  - Find bugs by querying `gitlab-org` for the appropriate group label ([SaaS](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_date&state=opened&label_name%5B%5D=group%3A%3Ahosted%20runners&first_page_size=20), [Core and Fleet](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_date&state=opened&label_name%5B%5D=group%3A%3Arunner&not%5Blabel_name%5D%5B%5D=FedRAMP%3A%3AVulnerability&first_page_size=20)).
  - See the [Handbook entry on Priority and Severity](https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/issue-triage/#priority) and the [Runner entry](https://about.gitlab.com/handbook/engineering/development/ops/verify/runner/#prioritization-labeling) for guidance on what values to assign.
- [ ] Tag individual team members or groups (e.g. `@gitlab-com/runner-group/core`, `@gitlab-com/runner-group/fleet` or `@gitlab-com/runner-group/saas`) in issues where you have questions or need additional input.
- [ ] Add a section to the weekly team call outlining issues that are high impact/severity or could benefit from a sync discussion by the team.

## 🥷 Merge Marauder 🏴‍☠️

- [ ] Scour the seas for [Community Contributor MRs](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=opened&draft=no&label_name[]=Community%20contribution&label_name[]=group%3A%3Arunner) in need of a captain. If they're anchored with a reviewer but lost in the fog of inactivity, it's your signal to swoop in and steer them to triumph!

## 🛟 Support & Security Responder 🚒

- [ ] Check for newly reported [CVE
vulnerabilities](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_date&state=opened&label_name%5B%5D=FedRAMP%3A%3AVulnerability&label_name%5B%5D=group%3A%3Arunner&not%5Blabel_name%5D%5B%5D=FedRAMP%3A%3ADR%20Status%3A%3AAccepted&not%5Blabel_name%5D%5B%5D=FedRAMP%3A%3ADR%20Status%3A%3AOpen&first_page_size=50)
attributed to our team. If there are any, then tag the EM, PM, any other relevant parties and make sure they are
included in the plan for the next milestone (regardless of severity, unless they are severity 1 in which case drop
everything and get them moving). Should be done only once during the week. See [here](https://handbook.gitlab.com/handbook/engineering/development/ops/verify/runner/#managing-cve-vulnerability-report-issues)
for process.
- [ ] Monday - Check for new [Support Help Requests](https://gitlab.com/gitlab-com/request-for-help/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Help%20group%3A%3Arunner&first_page_size=100) and work with the support engineer to resolve their problem.
- [ ] Tuesday - Check for new [Support Help Requests](https://gitlab.com/gitlab-com/request-for-help/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Help%20group%3A%3Arunner&first_page_size=100) and work with the support engineer to resolve their problem.
- [ ] Wednesday - Check for new [Support Help Requests](https://gitlab.com/gitlab-com/request-for-help/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Help%20group%3A%3Arunner&first_page_size=100) and work with the support engineer to resolve their problem.
- [ ] Thursday - Check for new [Support Help Requests](https://gitlab.com/gitlab-com/request-for-help/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Help%20group%3A%3Arunner&first_page_size=100) and work with the support engineer to resolve their problem.
- [ ] Friday - Check for new [Support Help Requests](https://gitlab.com/gitlab-com/request-for-help/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Help%20group%3A%3Arunner&first_page_size=100) and work with the support engineer to resolve their problem.

## 💬 Community Contribution Triager 📌

- [ ] Check for [new Community Contributions](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=opened&label_name[]=Community%20contribution&label_name[]=group%3A%3Arunner&reviewer_id=None) and find an appropriate reviewer. Do a first sanity check that they are ready for review and if not work with the MR Coaches to help the contributor.

# Duty Rotations

This issue is created each week by `@ajwalker`.

Contributions can be made at <https://gitlab.com/ajwalker/runner-team-task-issue-generator>.

## Fleet Bug Wrangler

Rotates {{range $idx, $val := .FleetBugWranglerRotation}}{{if $idx}}, {{end}}`{{ $val }}`{{end}}.

## Core Bug Wrangler

Rotates {{range $idx, $val := .CoreBugWranglerRotation}}{{if $idx}}, {{end}}`{{ $val }}`{{end}}.

## SaaS Bug Wrangler

Rotates {{range $idx, $val := .SaasBugWranglerRotation}}{{if $idx}}, {{end}}`{{ $val }}`{{end}}.

## Merge Marauder

Rotates {{range $idx, $val := .MergeMarauderRotation}}{{if $idx}}, {{end}}`{{ $val }}`{{end}}.

## All Other Duties

Other duties are rotated from a pool of Runner Core/SaaS/Fleet team members that haven't been assigned
duties that week.

/assign {{ .CoreBugWrangler }} {{ .FleetBugWrangler }} {{ .SupportResponder }} {{ .CommunityTriager }}

/labels ~"devops::verify" ~"group::runner" ~"section:ops"

/due {{ .Due }}

/cc @nicolewilliams

<!-- metadata:
{{ . | json }}
-->
